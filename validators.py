import datetime
import re


def null_validator(value):
    if value is None:
        return False, value

    return True, value


def email_validator(email):
    email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")

    if email is None or not re.match(email_regex, email):
        return False, email

    return True, email


def number_validator(number):
    if type(number) is not float and number is not None:
        return False, number

    return True, number


def date_validator(date):
    if type(date) is not datetime.datetime:
        return False, date

    return True, date


def bounced_validator(bounced):
    if bounced is None or (bounced != 0.0 and bounced != 1.0):
        return False

    return True, bounced
