import string
from datetime import datetime
import random
import pandas as pd
import sys as s
import bcrypt as bcrypt
import mysql.connector


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def is_in_box(l, box_id):
    for i in range(len(l)):
        if l[i][0] == box_id:
            return True
    return False


def get_mysql_connector():
    user_name = "alen"
    password = "313792897"
    cnx = mysql.connector.connect(user=user_name, password=password,
                                  host='aa1wu9gd26s32n0.c9is109chhto.eu-west-1.rds.amazonaws.com',
                                  database='testAlen')
    return cnx


def get_emails(cnx, df, query_start):
    for i in range(len(df)):
        if isinstance(df["email*"][i], float):
            continue

        query_start += "'"
        try:
            query_start += df["email*"][i].lower()
        except:
            print(i)
        query_start += "',"

    query_start = query_start[:-1]
    query_start += ")"

#    print(query_start)

    cursor = cnx.cursor()
    cursor.execute(query_start)
    emails_list = cursor.fetchall()
    cnx.commit()
    cursor.close()

    return emails_list


def enter_new_emails(cnx, df, emails_list):
    flag = 0

    flat_list = [item for sublist in emails_list for item in sublist]

    new_email_query = "INSERT INTO `users`(`email`, `password_hash`, `first_name`, `last_name`, `bounced`,  `image`, `birthday`, `gender`, `phone`,`weight`, `height`, `country`, `city`, `address`, `created_at`,`updated_at`) VALUES "

    for i in range(len(df)):

        if isinstance(df["email*"][i], float):
            continue
        e_pass = df["email*"][i].split("@", 1)[0].lower().encode('utf-8')

        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(e_pass, salt)

        if df["email*"][i] in flat_list:
            continue
        flag += 1
        new_email_query += "('"
        new_email_query += df["email*"][i]
        new_email_query += "', '"
        new_email_query += str(hashed)[2:-1]
        new_email_query += "', '"
        new_email_query += str(df["first_name*"][i])
        new_email_query += "', '"
        new_email_query += str(df["last_name*"][i])
        new_email_query += "', '0', 'defaultProfileImage.png', '"
        new_email_query += str(df["birthday(yyyy-mm-dd)"][i])
        new_email_query += "', '"
        new_email_query += str(df["gender*"][i])
        new_email_query += "', '"
        new_email_query += str(df["phone"][i])
        new_email_query += "', '60', '170', '"
        new_email_query += str(df["country"][i])
        new_email_query += "', '"
        new_email_query += str(df["city"][i])
        new_email_query += "', '"
        new_email_query += str(df["address"][i])
        new_email_query += "', '"
        new_email_query += str(datetime.now().strftime("%Y-%m-%d"))
        new_email_query += "', '"
        new_email_query += str(datetime.now().strftime("%Y-%m-%d"))
        new_email_query += "'),"

    new_email_query = new_email_query[:-1]

    print("Insert_New_Emails")

    if flag > 0:
        cursor = cnx.cursor()
        cursor.execute(new_email_query.replace("'nan'", "NULL"))
        cnx.commit()
        cursor.close()


def null_emails(cnx, df):
    pass_i_dict = {}

    null_emails_query = "INSERT INTO `users`(`email`, `password_hash`, `first_name`, `last_name`, `bounced`,  `image`, `birthday`, `gender`, `phone`,`weight`, `height`, `country`, `city`, `address`, `created_at`,`updated_at`) VALUES "

    for i in range(len(df)):

        if isinstance(df["email*"][i], float):
            null_pass = randomString(50)
            null_emails_query += "('"
            null_emails_query += str(df["email*"][i])
            null_emails_query += "', '"
            null_emails_query += null_pass
            null_emails_query += "', '"
            null_emails_query += str(df["first_name*"][i])
            null_emails_query += "', '"
            null_emails_query += str(df["last_name*"][i])
            null_emails_query += "', '0', 'defaultProfileImage.png', '"
            null_emails_query += str(df["birthday(yyyy-mm-dd)"][i])
            null_emails_query += "', '"
            null_emails_query += str(df["gender*"][i])
            null_emails_query += "', '"
            null_emails_query += str(df["phone"][i])
            null_emails_query += "', '60', '170', '"
            null_emails_query += str(df["country"][i])
            null_emails_query += "', '"
            null_emails_query += str(df["city"][i])
            null_emails_query += "', '"
            null_emails_query += str(df["address"][i])
            null_emails_query += "', '"
            null_emails_query += str(datetime.now().strftime("%Y-%m-%d"))
            null_emails_query += "', '"
            null_emails_query += str(datetime.now().strftime("%Y-%m-%d"))
            null_emails_query += "'),"

            pass_i_dict[null_pass] = i

    null_emails_query = null_emails_query[:-1]

    cursor = cnx.cursor()
    cursor.execute(null_emails_query.replace("'nan'", "NULL"))
    cnx.commit()
    cursor.close()

    return pass_i_dict


def null_password(cnx, null_users_dict, users_dict):
    pass_user_fk_dict = {}

    get_null_user_fk = "SELECT `password_hash` , `id` FROM users WHERE `email` IS NULL AND `password_hash` in ('"
    all_passwords = "','".join(null_users_dict.keys())
    get_null_user_fk += all_passwords
    get_null_user_fk += "')"

    print("Insert_Null_Users")

    cursor = cnx.cursor()
    cursor.execute(get_null_user_fk)
    pass_user_fk_list = cursor.fetchall()
    cnx.commit()
    cursor.close()

    for item in pass_user_fk_list:
        pass_user_fk_dict[item[0]] = item[1]

    for item in null_users_dict.keys():
        users_dict[null_users_dict[item]] = pass_user_fk_dict[item]

    # print(users_dict)

    return users_dict


def get_final_users_dict(cnx, df, users_dict):

    email_user_fk = "SELECT u.`email`, `id` FROM `users` u WHERE `email` in ('"

    for i in range(len(df)):
        if isinstance(df["email*"][i], float):
            continue

        try:
            email_user_fk += df["email*"][i].lower()
            email_user_fk += "','"

        except:
            print(i)

    email_user_fk = email_user_fk[:-2]
    email_user_fk += ")"

    # print(email_user_fk)

    cursor = cnx.cursor()
    cursor.execute(email_user_fk)
    add_to_dict = cursor.fetchall()
    cnx.commit()
    cursor.close()

    dict_email_as_key = {}

    for tuple in add_to_dict:
        dict_email_as_key[str(tuple[0]).lower()] = tuple[1]

    for i in range(len(df)):
        if isinstance(df["email*"][i], float):
            continue

        email = df["email*"][i].lower()
        users_dict[i] = dict_email_as_key[email]

    print("Create_Users_Dict")
    return users_dict

def users_boxes(cnx, df,users_dict, box_id, location_id):

    user_boxes_sqlquery = u'INSERT IGNORE INTO `users_boxes`(`user_fk`,`box_fk`,`locations_box_fk`,`first_name`, `last_name` ,`birthday`, `gender`, `phone`, `additional_phone`,`weight`, `height`, `personal_id`,`country`, `city`, `address`, `active`, `medical_cert`,`created_at`,`updated_at`, `rfid`) VALUES '

    for i in range(len(df)):
        user_boxes_sqlquery += "("
        user_boxes_sqlquery += str(users_dict[i])
        user_boxes_sqlquery += ",'"
        user_boxes_sqlquery += str(box_id)
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(location_id)
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["first_name*"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["last_name*"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["birthday(yyyy-mm-dd)"][i])
        user_boxes_sqlquery += "','"
        if df["gender*"][i] == u'male' or df["gender*"][i] == u'Male':
            user_boxes_sqlquery += u'male'
        elif df["gender*"][i] == u'female' or df["gender*"][i] == u'Female':
            user_boxes_sqlquery += u'female'
        else:
            print("Error in Gender in mail:" + str(df["email*"][i]))
        user_boxes_sqlquery += "','"
        if str(df["phone"][i]).startswith("5"):
            user_boxes_sqlquery += str('0')
        user_boxes_sqlquery += str(df["phone"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["additional_phone"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(60)
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(170)
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["personal_id"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["country"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["city"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["address"][i])
        user_boxes_sqlquery += "','"

        membership_end_date = df["membership_end_date(yyyy-mm-dd)"][i]

        if type(membership_end_date) is not str:
            user_boxes_sqlquery += "1"
        else:
            if datetime.strptime(membership_end_date, '%Y-%m-%d') < datetime.now():
                user_boxes_sqlquery += "0"
            else:
                user_boxes_sqlquery += "1"
        # user_boxes_sqlquery+=str(1)
        user_boxes_sqlquery += "','"
        # user_boxes_sqlquery+=str(df["medical_cert"][i])
        user_boxes_sqlquery += str(0)
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["ub_created_at"][i])
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(datetime.now().strftime("%Y-%m-%d"))
        user_boxes_sqlquery += "','"
        user_boxes_sqlquery += str(df["rfid"][i])
        user_boxes_sqlquery += "'),"

    user_boxes_sqlquery = user_boxes_sqlquery[:-1]

    print("Insert_Users_Boxes")

    cursor = cnx.cursor()
    cursor.execute(user_boxes_sqlquery.replace("'nan'", "NULL"), )
    cnx.commit()
    cursor.close()


def users_roles(cnx,df,users_dict,box_id):

    user_roles_sqlquery = "INSERT IGNORE INTO `users_roles`(`user_fk`,`role_fk`,`box_fk`,`created_at`,`updated_at`) VALUES"

    for i in range(len(df)):
        user_roles_sqlquery+="("
        user_roles_sqlquery+=str(users_dict[i])
        user_roles_sqlquery+=","
        user_roles_sqlquery+="3"
        user_roles_sqlquery+= ","
        user_roles_sqlquery+=str(box_id)
        user_roles_sqlquery += ",'"
        user_roles_sqlquery += str(datetime.now().strftime("%Y-%m-%d"))
        user_roles_sqlquery += "','"
        user_roles_sqlquery += str(datetime.now().strftime("%Y-%m-%d"))
        user_roles_sqlquery += "'),"

    user_roles_sqlquery = user_roles_sqlquery[:-1]

    print("Insert_Users_Roles")
    cursor = cnx.cursor()
    cursor.execute(user_roles_sqlquery.replace("'nan'", "NULL"), )
    cnx.commit()
    cursor.close()

def membership_users(cnx, df, users_dict,box_id):

    membership_sqlquery = u'INSERT INTO `membership_user`(`user_fk`,`start`,`end`,`membership_type_fk`,`sessions_left`,`price`,`debt`,`box_fk`, `active`,`created_at`,`updated_at`) VALUES '

    for i in range(len(df)):
        membership_sqlquery += "("
        membership_sqlquery += "'"
        membership_sqlquery += str(users_dict[i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["membership_start_date(yyyy-mm-dd)"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["membership_end_date(yyyy-mm-dd)"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["membership_type"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["left"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["price"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(df["debt"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(box_id)
        membership_sqlquery += "','"

        membership_end_date = df["membership_end_date(yyyy-mm-dd)"][i]

        if type(membership_end_date) is not str:
            membership_sqlquery += str(1)
        else:
            if datetime.strptime(membership_end_date, '%Y-%m-%d') < datetime.now():
                membership_sqlquery += str(0)
            else:
                membership_sqlquery += str(1)
        membership_sqlquery += "','"
        membership_sqlquery += str(df["mu_created_at"][i])
        membership_sqlquery += "','"
        membership_sqlquery += str(datetime.now().strftime("%Y-%m-%d"))
        membership_sqlquery += "'),"

    membership_sqlquery = membership_sqlquery[:-1]


    print("Insert Membership_User")
    cursor = cnx.cursor()
    cursor.execute(membership_sqlquery.replace("'nan'", "NULL"), )
    cnx.commit()
    cursor.close()


def main():
    cnx = get_mysql_connector()
    file_name = s.argv[1]
    box_id = int(s.argv[2])
    location_id = s.argv[3]
    injury_flag = s.argv[5]
    users_dict = {}

    df = pd.read_csv(file_name, encoding="utf8", dtype="unicode")

    enter_new_emails(cnx, df, get_emails(cnx, df, 'SELECT u.`email` FROM `users` u WHERE `email` in ('))
    null_password(cnx, null_emails(cnx, df), users_dict)
    get_final_users_dict(cnx, df, users_dict)
    users_boxes(cnx, df,users_dict, box_id, location_id)
    users_roles(cnx,df, users_dict, box_id)
    membership_users(cnx, df, users_dict, box_id)
    print("All Done !!!")


main()
