from openpyxl import Workbook, load_workbook
import logging
import yaml
import validators
import datetime

logging.basicConfig(filename='validation.log', level=logging.DEBUG)

# OUTPUTS WORKBOOKS & WORKSHEETS
workbook_valid_information = Workbook()
worksheet_valid_information = workbook_valid_information.active
worksheet_valid_information.title = "valid_members"
worksheet_valid_information_index = 1

workbook_exceptions = Workbook()
worksheet_exceptions = workbook_exceptions.active
worksheet_exceptions.title = "exceptions"
worksheet_exceptions_row_index = 1


def init_output_worksheets(headlines_list):
    copy_row_to_valid_members_excel(headlines_list)
    copy_row_to_exceptions_excel(headlines_list)


def run_excel_validations(columns_configuration):
    for row in worksheet_to_validate.iter_rows(min_row=2, values_only=True):
        is_row_valid = True
        row_to_log = list(row)
        row_error_log = ""

        for col_index in range(0, len(row)):
            # Get all validators from configuration
            cell_validators = columns_configuration[col_index]['validations']

            # Run all validations for the current cell
            is_cell_valid, new_value, error_log = run_cell_validations(cell_validators, row[col_index])

            # Update raw validity
            is_row_valid = is_row_valid and is_cell_valid

            if is_cell_valid:
                row_to_log[col_index] = new_value
            else:
                error_str = "Column " + str(
                    columns_configuration[col_index]['name']) + " is invalid. Error log: " + error_log + "\n"
                row_error_log += error_str
                logging.debug(error_str)

        # Append output sheets
        if is_row_valid:
            copy_row_to_valid_members_excel(row_to_log)
        else:
            logging.exception(row_to_log)
            row_to_log.append(row_error_log)
            copy_row_to_exceptions_excel(row_to_log)


def run_cell_validations(all_validators, value):
    is_valid = True
    new_value = value
    error_log = ""

    for validator in all_validators:
        function_validator = getattr(validators, validator)
        is_cell_valid, new_value = function_validator(new_value)
        is_valid = is_valid and is_cell_valid

        if not is_cell_valid:
            error_log += "The validator: " + validator + " has failed. "

    return is_valid, new_value, error_log


def copy_row_to_valid_members_excel(row):
    global worksheet_valid_information_index

    for col_index in range(0, len(row)):
        worksheet_valid_information.cell(worksheet_valid_information_index, col_index + 1).value = row[col_index]

    worksheet_valid_information_index += 1


def copy_row_to_exceptions_excel(row):
    global worksheet_exceptions_row_index

    for col_index in range(0, len(row)):
        worksheet_exceptions.cell(worksheet_exceptions_row_index, col_index + 1).value = row[col_index]

    worksheet_exceptions_row_index += 1


if __name__ == '__main__':
    logging.debug("*************************************************************************************")
    now = datetime.datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    logging.debug("Starting to validate..." + dt_string)
    logging.debug("*************************************************************************************")

    # loading Excel sheet
    workbook_to_validate = load_workbook(filename='Shitty Excel.xlsx')
    worksheet_to_validate = workbook_to_validate.active

    # initiate columns from yaml -> new
    columns_validations_configuration = open("validations_config.yaml")
    all_columns_information = yaml.load(columns_validations_configuration, Loader=yaml.FullLoader)

    # initiate output sheets with the columns headlines
    headlines_list = []
    for key in all_columns_information.keys():
        headlines_list.append(key)

    init_output_worksheets(headlines_list)

    # Switching keys to be the indexes, pushing column names to value
    for index in range(0, len(worksheet_to_validate[1])):
        name = worksheet_to_validate[1][index].value
        all_columns_information[name]['name'] = name
        all_columns_information[index] = all_columns_information.pop(name)

    run_excel_validations(all_columns_information)

    # save to new files
    workbook_exceptions.worksheets[0] = worksheet_exceptions
    workbook_exceptions.save("members_exceptions.xlsx")

    workbook_valid_information.worksheets[0] = worksheet_valid_information
    workbook_valid_information.save("valid_members.xlsx")

    logging.debug("*************************************************************************************")
    logging.debug("End Of Validation")
    logging.debug("*************************************************************************************")
